import axios from 'axios';

const instance = axios.create({
    baseURL: process.env.VUE_APP_PICSUM_API_URL
});

export default instance;