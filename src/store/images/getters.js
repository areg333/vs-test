export function images(state) {
    return state.images;
}

export function getNextPage(state) {
    return state.nextPage
}

export function getCurrentPage(state) {
    return state.currentPage
}

export function getPrevPage(state) {
    return state.prevPage
}

export function getPrevPageLabel(state) {
    return state.prevPageLabel
}

export function getNextPageLabel(state) {
    return state.nextPageLabel
}

export function limit(state) {
    return state.limit
}

export function selectedImage(state) {
    return state.selectedImage
}
