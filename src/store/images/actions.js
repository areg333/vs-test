import axios from '../../boot/axios';
import * as helper from '../../Helpers/helper';
import _ from 'lodash';

export async function fetchImages(context, {limit, page}) {
    const imageWidth = process.env.VUE_APP_GRID_IMAGE_WIDTH;
    const imageHeight = process.env.VUE_APP_GRID_IMAGE_HEIGHT;
    const _page = page !== null ? parseInt(page) : 1;
    const queryParams = '?page=' + _page + '&limit=' + limit;

    await context.commit('SET_PER_PAGE_LIMIT', limit);

    // Fetch images from the server
    await axios.get('list' + queryParams)
        .then(response => {
            if (response) {
                // Call another action for configuration of pagination
                context.dispatch('configurePagination', _.get(response, 'headers.link'));

                //configure the result of images
                return response.data.map(function (item) {
                    let path = item.download_url;

                    // Removing image width and height from the url
                    path = path.substring(0, path.lastIndexOf('/'));
                    path = path.substring(0, path.lastIndexOf('/'));

                    path += '/' + imageWidth + '/' + imageHeight;

                    return {
                        ...item,
                        width: imageWidth,
                        height: imageHeight,
                        download_url: path
                    }
                });
            }
        })
        .then(result => {
            context.commit('SET_IMAGES', result)
        })
        .catch(error => {
            // TODO: implement HOC for handling global errors.
            console.error('Failed to get data from the server', error);
        });
}

export async function configurePagination(context, paginationString) {
    const paginationStringsCollection = _.split(paginationString, ',');
    let pagesUrl = [];
    let currentPage = 1;
    let nextPageLabel = null;
    let prevPageLabel = null;

    paginationStringsCollection.forEach(function (string_url) {
        pagesUrl.push(helper.transformStringToPaginationAttr(_.split(string_url, ';')))
    });

    pagesUrl.forEach(function (page_url) {
        let pageQuery;
        let page;
        let rel;

        if (_.get(page_url, '0.page_url')) {
            pageQuery = helper.getParsedQuery(page_url[0].page_url);
            page = parseInt(pageQuery.query.page);
            rel = page_url[0].rel;
        }

        switch (rel) {
            case 'next':
                context.commit('SET_NEXT_PAGE', page);
                currentPage = parseInt(pageQuery.query.page) - 1;
                nextPageLabel = rel;
                break;

            case 'prev':
                context.commit('SET_PREV_PAGE', page);
                currentPage = parseInt(pageQuery.query.page) + 1;
                prevPageLabel = rel;
                break;
        }
    });

    context.commit('SET_CURRENT_PAGE', currentPage);
    context.commit('SET_NEXT_PAGE_LABEL', nextPageLabel);
    context.commit('SET_PREV_PAGE_LABEL', prevPageLabel);
}
