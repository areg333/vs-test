export default function () {
    return {
        images: [],
        nextPage: null,
        prevPage: null,
        nextPageLabel: null,
        prevPageLabel: null,
        currentPage: null,
        limit: process.env.VUE_APP_GRID_IMAGES_DEFAULT_LIMIT
    }
}
