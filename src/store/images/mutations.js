export function SET_IMAGES(state, images) {
    state.images = images;
}
export function SET_PREV_PAGE(state, prevPage) {
    state.prevPage = prevPage
}
export function SET_NEXT_PAGE(state, nextPage) {
    state.nextPage = nextPage
}
export function SET_PREV_PAGE_LABEL(state, prevPageLabel) {
    state.prevPageLabel = prevPageLabel
}
export function SET_NEXT_PAGE_LABEL(state, nextPageLabel) {
    state.nextPageLabel = nextPageLabel
}
export function SET_CURRENT_PAGE(state, currentPage) {
    state.currentPage = currentPage
}
export function SET_PER_PAGE_LIMIT(state, limit) {
    state.limit = limit
}
export function SET_SELECTED_IMAGE(state, image) {
    state.selectedImage = image
}
