const routes = [
    {
        mode: 'history',
        path: '/',
        redirect: {name: 'Home'},
        component: () => import('../layouts/MainLayout.vue'),
        children: [
            {
                path: '/home',
                name: 'Home',
                component: () => import('../pages/Home.vue')
            },
            {
                path: '/image/:id',
                name: 'Image',
                component: () => import('../pages/SingleImage.vue')
            }
        ]
    },
    {
        path: '*',
        component: () => import('../pages/Error404.vue')
    }
];

export default routes;
